VideoSkip API
=============

## Development

You will need postgres running. A simple way is to run it with docker:

```bash
docker run -p 5432:5432 -v /var/tmp/vsdb:/var/lib/postgresql/data -d postgres
```

This project uses Python 3.8.

### Set up

```bash
pip install pipenv
pipenv install --dev
cp videoskip/settings_dev.py videoskip/settings.py
pipenv run ./manage.py migrate
pipenv run ./manage.py loaddata init
pipenv run ./manage.py createsuperuser
```

### Run server

```bash
pipenv run ./manage.py runserver
```

### Run tests

```bash
pipenv run ./manage.py test --noinput --settings videoskip.settings_test api.tests
```

## Permissions

Objects that can be created by users such as videos, timestamps, services,
and time types all have a user field that describes the user who created the object.
This allows for better tracking of users, either normal or malicious.
Because of this, I opted to avoid installing an object level permissions library
and just utilize the user field for permission checking of objects.
This also allows users to "show off" contributions and
automatically promote users to moderator status.

### Regular users

**Videos** and to an extent **Timestamp revisions**: view, add, and change freely.
Delete by submitter only (in case of user mistake).

**Services** and **Time types**: view freely. Add and change must be approved by a moderator.
There is no delete permissions for users.

**Mod queue**: View, change, and delete their own request only.
A request is added when submitting a change for services and timestamps.

### Moderator group

Permissions (on top of regular user permissions):

**Mod queue**: view and add (submitters can change and delete their own requests)

**Services** and **Time types**: add, change, and delete

**Videos** and **Timestamp revisions**: delete

**Users**: delete (this is only for custom actions, not for explicitly deleting users)

(All moderator changes are logged)

## Mod queue

Moderators can approve of requests to add or change certain models.
However, they cannot change or delete requests.
