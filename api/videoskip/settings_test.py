from .settings_base import *

SECRET_KEY = 'test'

DEBUG = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'postgres',
        'USER': 'postgres',
        'HOST': os.environ.get('DB_HOST', 'localhost'),
        'PORT': 5432,
    }
}

CLIENT_URL = 'http://localhost:3000'
