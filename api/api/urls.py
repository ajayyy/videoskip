from django.urls import include, path
from rest_framework import routers

from . import views

router = routers.DefaultRouter()
router.register(r'services', views.ServiceViewSet)
router.register(r'time-types', views.TimeTypeViewSet)
router.register(r'timestamp-revisions', views.TimestampRevisionViewSet)
router.register(r'videos', views.VideoViewSet)
router.register(r'users', views.UserViewSet)
router.register(r'mod-queue', views.ModQueueViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('rest-auth/', include('rest_auth.urls')),
    path('rest-auth/registration/', include('rest_auth.registration.urls')),
    path('accounts/', include('allauth.urls')),
]
