from django.contrib import admin
from django.contrib.admin.models import LogEntry

from . import models


class TimestampInline(admin.TabularInline):
    show_change_link = True
    model = models.Timestamp


@admin.register(LogEntry)
class LogEntryAdmin(admin.ModelAdmin):
    readonly_fields = ('content_type', 'user', 'action_time', 'object_id',
                       'object_repr', 'action_flag', 'change_message')


admin.site.register(models.Service)
admin.site.register(models.TimeType)
admin.site.register(models.TimestampRevision, inlines=[TimestampInline])
admin.site.register(models.Video)
admin.site.register(models.ModQueue)
