"""videoskip URL Configuration"""

from django.contrib import admin
from django.urls import path, include
from django.views.generic.base import RedirectView

from django.conf import settings

urlpatterns = [
    path('accounts/login/', RedirectView.as_view(
        url=settings.CLIENT_URL + '/sign-in?success')),
    path('accounts/', include('django.contrib.auth.urls')),
    path('api/admin/', admin.site.urls),
    path('api/api-auth/', include('rest_framework.urls')),
    path('api/', include('api.urls')),
    path('api', RedirectView.as_view(url='api/')),
    path('', RedirectView.as_view(url='api/')),
]
