from django_filters import rest_framework as filters

from . import models


class ModQueueFilter(filters.FilterSet):
    # use date instead of user in case user is deleted
    awaiting = filters.BooleanFilter(
        label='Awaiting approval', field_name='approved_on',
        lookup_expr='isnull')

    class Meta:
        model = models.ModQueue
        fields = ('submitted_by', 'approved_by', 'approved')
