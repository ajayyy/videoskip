from django.contrib.admin.models import ADDITION
from django.contrib.auth.models import User, Group, Permission
from django.contrib.contenttypes.models import ContentType
from django.test import TestCase, RequestFactory
from django.urls import reverse
from rest_framework.test import APITestCase

from . import models
from . import serializers


class TimestampRevisionTestCase(TestCase):
    def setUp(self):
        super().setUp()

        self.service = models.Service.objects.create(
            name='test', description='test')

    def test_removal(self):
        """Check that a revision removed updates prev and next"""
        rev1 = models.TimestampRevision.objects.create()
        rev2 = models.TimestampRevision.objects.create(previous_revision=rev1)
        rev3 = models.TimestampRevision.objects.create(previous_revision=rev2)

        video = models.Video.objects.create(
            video_id='test', service=self.service, timestamp_revision=rev2)

        rev1.video = rev2.video = rev3.video = video
        rev1.save()
        rev2.save()
        rev3.save()

        # sanity check
        self.assertEqual(rev1.next_revision, rev2)
        self.assertEqual(rev2.next_revision, rev3)

        # remove rev2, and see if rev1 -> rev3 and rev3 -> rev1
        rev2.delete()
        self.assertEqual(rev1.next_revision, rev3)
        self.assertEqual(rev3.previous_revision, rev1)

        # sanity check that signal does not break
        rev1.delete()
        rev3.delete()

    def test_removal_video(self):
        """Check that a revision removed updates video rev"""
        rev1 = models.TimestampRevision.objects.create()
        rev2 = models.TimestampRevision.objects.create(previous_revision=rev1)

        video = models.Video.objects.create(
            video_id='test', service=self.service, timestamp_revision=rev2)

        rev1.video = rev2.video = video
        rev1.save()
        rev2.save()

        # remove rev2, and see if video.rev is rev1
        rev2.delete()
        self.assertEqual(video.timestamp_revision, rev1)

        # sanity check that signal does not break revision
        rev1.delete()


class VideoSerializerTestCase(TestCase):
    def setUp(self):
        super().setUp()

        user = User.objects.create_user(
            username='test', email='test@test', password='test')

        request_factory = RequestFactory()
        self.get_request = request_factory.get('/')
        self.post_request = request_factory.post('/')
        self.put_request = request_factory.put('/')

        self.post_request.user = self.put_request.user = user

        self.service = models.Service.objects.create(
            name='test', description='test')
        self.serviceSerializer = serializers.ServiceSerializer(
            instance=self.service, context={'request': self.get_request})

    def make_post(self):
        return serializers.VideoSerializer(data={
            'video_id': 'test',
            'service': self.serviceSerializer.data['url'],
            'timestamps': [
                {
                    'time_type': None,
                    'start': 2,
                    'end': 3
                },
                {
                    'time_type': None,
                    'start': 0,
                    'end': 1
                }
            ]
        }, context={'request': self.post_request})

    def make_put(self, id):
        video = models.Video.objects.get(id=id)

        return serializers.VideoSerializer(data={
            'video_id': 'test',
            'service': self.serviceSerializer.data['url'],
            'timestamps': [
                {
                    'time_type': None,
                    'start': 3,
                    'end': 4
                },
                {
                    'time_type': None,
                    'start': 0,
                    'end': 2
                }
            ]
        }, instance=video, context={'request': self.put_request})

    def test_timestamp_revision_updates(self):
        """Validate timestamp revisions are added"""
        # Test POST
        serializer = self.make_post()
        serializer.is_valid(raise_exception=True)
        serializer.save()

        video = models.Video.objects.get(id=serializer.data['id'])
        self.assertEqual(video.timestamp_revision.timestamps.first().end, 1)

        # Test PUT
        serializer = self.make_put(video.id)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        video = models.Video.objects.get(id=serializer.data['id'])
        self.assertEqual(video.timestamp_revision.timestamps.first().end, 2)

    def test_timestamp_order(self):
        """Validate timestamps are inserted in order"""
        # Test POST
        serializer = self.make_post()
        serializer.is_valid(raise_exception=True)
        serializer.save()

        video = models.Video.objects.get(id=serializer.data['id'])
        timestamps = video.timestamp_revision.timestamps.all()
        self.assertLess(timestamps[0].start, timestamps[1].start)

        # Test PUT
        serializer = self.make_put(serializer.data['id'])
        serializer.is_valid(raise_exception=True)
        serializer.save()

        video = models.Video.objects.get(id=serializer.data['id'])
        timestamps = video.timestamp_revision.timestamps.all()
        self.assertLess(timestamps[0].start, timestamps[1].start)

    def test_revision_with_video(self):
        """Validate revisions have videos attached"""
        # Test POST
        serializer = self.make_post()
        serializer.is_valid(raise_exception=True)
        serializer.save()

        video = models.Video.objects.get(id=serializer.data['id'])
        self.assertIsNotNone(video.timestamp_revision.video)

        # Test PUT
        serializer = self.make_put(video.id)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        video = models.Video.objects.get(id=serializer.data['id'])
        self.assertIsNotNone(video.timestamp_revision.video)

    def test_validation_video_exists(self):
        """Test validation error when video already exists"""
        serializer = self.make_post()
        serializer.is_valid(raise_exception=True)
        serializer.save()

        serializer = self.make_post()
        self.assertFalse(serializer.is_valid())

    def test_validation_start_less_than_end(self):
        """Test validation error when start time is less than end time"""
        serializer = serializers.VideoSerializer(data={
            'video_id': 'test',
            'service': self.serviceSerializer.data['url'],
            'timestamps': [
                {
                    'time_type': None,
                    'start': 3,
                    'end': 2
                },
            ]
        }, context={'request': self.post_request})

        self.assertFalse(serializer.is_valid())

    def test_validation_no_timestamps(self):
        """Test validation error when there is no timestamps key"""
        serializer = serializers.VideoSerializer(data={
            'video_id': 'test',
            'service': self.serviceSerializer.data['url'],
        }, context={'request': self.post_request})

        self.assertFalse(serializer.is_valid())

    def test_validation_timestamps_empty(self):
        """Test validation error when timestamps is empty"""
        serializer = serializers.VideoSerializer(data={
            'video_id': 'test',
            'service': self.serviceSerializer.data['url'],
            'timestamps': []
        }, context={'request': self.post_request})

        self.assertFalse(serializer.is_valid())

    def test_validation_no_time_type(self):
        """Test validation error when there is not time type key"""
        serializer = serializers.VideoSerializer(data={
            'video_id': 'test',
            'service': self.serviceSerializer.data['url'],
            'timestamps': [{
                'start': 0,
                'end': 1
            }]
        }, context={'request': self.post_request})

        self.assertFalse(serializer.is_valid())


class VideoPermissionsTestCase(APITestCase):
    def setUp(self):
        super().setUp()

        self.owner = User.objects.create_user(
            username='owner', email='test@test', password='test')
        self.user = User.objects.create_user(
            username='test', email='test@test', password='test')

        request_factory = RequestFactory()
        self.get_request = request_factory.get('/')

        self.service = models.Service.objects.create(
            name='test', description='test')
        self.timestamp_revision = models.TimestampRevision.objects.create(
            user=self.owner)
        models.Timestamp.objects.create(
            start=0, end=1,
            timestamp_revision=self.timestamp_revision)
        self.video = models.Video.objects.create(
            video_id='test', service=self.service,
            timestamp_revision=self.timestamp_revision,
            user=self.owner)

    def test_anonymous(self):
        response = self.client.post(reverse('video-list'))
        self.assertEqual(response.status_code, 401)

        response = self.client.put(reverse('video-list'))
        self.assertEqual(response.status_code, 401)

        response = self.client.delete(
            reverse('video-detail', args=(self.video.id,)))
        self.assertEqual(response.status_code, 401)

    def test_delete_by_owner(self):
        self.client.login(username=self.owner.username, password='test')
        response = self.client.delete(
            reverse('video-detail', args=(self.video.id,)))
        self.assertEqual(response.status_code, 204)

    def test_delete_by_other(self):
        self.client.login(username=self.user.username, password='test')
        response = self.client.delete(
            reverse('video-detail', args=(self.video.id,)))
        self.assertEqual(response.status_code, 403)

    def test_modify_by_other(self):
        self.client.login(username=self.user.username, password='test')
        serializer = serializers.VideoSerializer(
            instance=self.video, context={'request': self.get_request})
        response = self.client.put(
            reverse('video-detail', args=(self.video.id,)),
            data=serializer.data, format='json')
        self.assertEqual(response.status_code, 200)


class TimestampRevisionPermissionsTestCase(APITestCase):
    def setUp(self):
        super().setUp()

        self.owner = User.objects.create_user(
            username='owner', email='test@test', password='test')
        self.user = User.objects.create_user(
            username='test', email='test@test', password='test')

        self.service = models.Service.objects.create(
            name='test', description='test')
        self.timestamp_revision = models.TimestampRevision.objects.create(
            user=self.owner)

    def test_delete_by_owner(self):
        self.client.login(username=self.owner.username, password='test')
        response = self.client.delete(
            reverse('timestamprevision-detail', args=(self.timestamp_revision.id,)))
        self.assertEqual(response.status_code, 204)

    def test_delete_by_other(self):
        self.client.login(username=self.user.username, password='test')
        response = self.client.delete(
            reverse('timestamprevision-detail', args=(self.timestamp_revision.id,)))
        self.assertEqual(response.status_code, 403)

    def test_modify(self):
        self.client.login(username=self.owner.username, password='test')
        response = self.client.put(
            reverse('timestamprevision-detail', args=(self.timestamp_revision.id,)))
        self.assertEqual(response.status_code, 403)

        self.client.login(username=self.user.username, password='test')
        response = self.client.put(
            reverse('timestamprevision-detail', args=(self.timestamp_revision.id,)))
        self.assertEqual(response.status_code, 403)


class ServicePermissionsTestCase(APITestCase):
    def setUp(self):
        super().setUp()

        self.owner = User.objects.create_user(
            username='owner', email='test@test', password='test')
        self.user = User.objects.create_user(
            username='test', email='test@test', password='test')

        request_factory = RequestFactory()
        self.get_request = request_factory.get('/')

        self.service = models.Service.objects.create(
            name='test', description='test', service_id='test', user=self.owner)

    def test_delete_by_owner(self):
        self.client.login(username=self.owner.username, password='test')
        response = self.client.delete(
            reverse('service-detail', args=(self.service.id,)))
        self.assertEqual(response.status_code, 204)

    def test_delete_by_other(self):
        self.client.login(username=self.user.username, password='test')
        response = self.client.delete(
            reverse('service-detail', args=(self.service.id,)))
        self.assertEqual(response.status_code, 403)

    def test_modify_by_other(self):
        self.client.login(username=self.user.username, password='test')
        serializer = serializers.ServiceSerializer(
            instance=self.service, context={'request': self.get_request})
        response = self.client.put(
            reverse('service-detail', args=(self.service.id,)),
            data=serializer.data, format='json')
        self.assertEqual(response.status_code, 200)


class TimeTypePermissionsTestCase(APITestCase):
    def setUp(self):
        super().setUp()

        self.owner = User.objects.create_user(
            username='owner', email='test@test', password='test')
        self.user = User.objects.create_user(
            username='test', email='test@test', password='test')

        request_factory = RequestFactory()
        self.get_request = request_factory.get('/')

        self.time_type = models.TimeType.objects.create(
            name='test', description='test', user=self.owner)

    def test_delete_by_owner(self):
        self.client.login(username=self.owner.username, password='test')
        response = self.client.delete(
            reverse('timetype-detail', args=(self.time_type.id,)))
        self.assertEqual(response.status_code, 204)

    def test_delete_by_other(self):
        self.client.login(username=self.user.username, password='test')
        response = self.client.delete(
            reverse('timetype-detail', args=(self.time_type.id,)))
        self.assertEqual(response.status_code, 403)

    def test_modify_by_other(self):
        self.client.login(username=self.user.username, password='test')
        serializer = serializers.TimeTypeSerializer(
            instance=self.time_type, context={'request': self.get_request})
        response = self.client.put(
            reverse('timetype-detail', args=(self.time_type.id,)),
            data=serializer.data, format='json')
        self.assertEqual(response.status_code, 200)


class ServiceSerializerTestCase(APITestCase):
    def setUp(self):
        super().setUp()

        self.moderator = User.objects.create_user(
            username='mod', email='test@test', password='test')
        self.user = User.objects.create_user(
            username='test', email='test@test', password='test')
        self.mod_group = Group.objects.create(name='Moderator')
        self.moderator.groups.add(self.mod_group)

    def test_add(self):
        # Check that a user gets returned a ModQueue
        self.client.login(username=self.user.username, password='test')
        response = self.client.post(
            reverse('service-list'),
            data={'service_id': 'test', 'name': 'test', 'description': 'test'},
            format='json')
        self.assertEqual(response.status_code, 201)
        self.assertIn('mod-queue', response.data['url'])

        # Check that moderator bypasses mod queue
        self.client.login(username=self.moderator.username, password='test')
        response = self.client.post(
            reverse('service-list'),
            data={'service_id': 'test', 'name': 'test', 'description': 'test'},
            format='json')
        self.assertEqual(response.status_code, 201)
        self.assertIn('service', response.data['url'])

    def test_change(self):
        # Check that a user gets returned a ModQueue
        self.client.login(username=self.user.username, password='test')
        service = models.Service.objects.create(
            name='test', description='test')
        response = self.client.put(
            reverse('service-detail', args=(service.id,)),
            data={
                'service_id': 'test',
                'name': 'test2',
                'description': 'test2'
            },
            format='json')
        self.assertEqual(response.status_code, 200)
        self.assertIn('mod-queue', response.data['url'])

        # Check that moderator bypasses mod queue
        self.client.login(username=self.moderator.username, password='test')
        response = self.client.put(
            reverse('service-detail', args=(service.id,)),
            data={
                'service_id': 'test',
                'name': 'test2',
                'description': 'test2'
            },
            format='json')
        self.assertEqual(response.status_code, 200)
        self.assertIn('service', response.data['url'])


class TimeTypeSerializerTestCase(APITestCase):
    def setUp(self):
        super().setUp()

        self.moderator = User.objects.create_user(
            username='mod', email='test@test', password='test')
        self.user = User.objects.create_user(
            username='test', email='test@test', password='test')
        self.mod_group = Group.objects.create(name='Moderator')
        self.moderator.groups.add(self.mod_group)

    def test_add(self):
        # Check that a user gets returned a ModQueue
        self.client.login(username=self.user.username, password='test')
        response = self.client.post(
            reverse('timetype-list'),
            data={'name': 'test', 'description': 'test'},
            format='json')
        self.assertEqual(response.status_code, 201)
        self.assertIn('mod-queue', response.data['url'])

        # Check that moderator bypasses mod queue
        self.client.login(username=self.moderator.username, password='test')
        response = self.client.post(
            reverse('timetype-list'),
            data={'name': 'test', 'description': 'test'},
            format='json')
        self.assertEqual(response.status_code, 201)
        self.assertIn('time-type', response.data['url'])

    def test_change(self):
        # Check that a user gets returned a ModQueue
        self.client.login(username=self.user.username, password='test')
        time_type = models.TimeType.objects.create(
            name='test', description='test')
        response = self.client.put(
            reverse('timetype-detail', args=(time_type.id,)),
            data={
                'name': 'test2',
                'description': 'test2'
            },
            format='json')
        self.assertEqual(response.status_code, 200)
        self.assertIn('mod-queue', response.data['url'])

        # Check that moderator bypasses mod queue
        self.client.login(username=self.moderator.username, password='test')
        response = self.client.put(
            reverse('timetype-detail', args=(time_type.id,)),
            data={
                'name': 'test2',
                'description': 'test2'
            },
            format='json')
        self.assertEqual(response.status_code, 200)
        self.assertIn('time-type', response.data['url'])


class ModQueueViewSetTestCase(APITestCase):
    def setUp(self):
        super().setUp()

        self.moderator = User.objects.create_user(
            username='mod', email='test@test', password='test')
        self.user = User.objects.create_user(
            username='test', email='test@test', password='test')
        self.other_user = User.objects.create_user(
            username='test2', email='test@test', password='test')
        mod_group = Group.objects.create(name='Moderator')
        view = Permission.objects.get(name='Can view mod queue')
        add = Permission.objects.get(name='Can add mod queue')
        mod_group.permissions.add(view)
        mod_group.permissions.add(add)
        self.moderator.groups.add(mod_group)

    def test_get(self):
        content_type = ContentType.objects.get(
            app_label='api', model='service')
        request = models.ModQueue.objects.create(
            content_type=content_type,
            request_type=ADDITION,
            request={},
            submitted_by=self.user)

        # Check that anonymous users can't access mod requests
        response = self.client.get(
            reverse('modqueue-detail', args=(request.id,)))
        self.assertEqual(response.status_code, 403)
        response = self.client.get(reverse('modqueue-list'))
        self.assertEqual(response.status_code, 403)

        # Check that other users can't access other users mod requests
        self.client.login(username=self.other_user.username, password='test')
        response = self.client.get(
            reverse('modqueue-detail', args=(request.id,)))
        self.assertEqual(response.status_code, 404)
        response = self.client.get(reverse('modqueue-list'))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data['results']), 0)

        # Check that the submitter can access their request
        self.client.login(username=self.user.username, password='test')
        response = self.client.get(
            reverse('modqueue-detail', args=(request.id,)))
        self.assertEqual(response.status_code, 200)
        response = self.client.get(reverse('modqueue-list'))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data['results']), 1)

        # Check that moderators can access mod requests
        self.client.login(username=self.moderator.username, password='test')
        response = self.client.get(
            reverse('modqueue-detail', args=(request.id,)))
        self.assertEqual(response.status_code, 200)
        response = self.client.get(reverse('modqueue-list'))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data['results']), 1)

    def test_post(self):
        content_type = ContentType.objects.get(
            app_label='api', model='service')
        request = models.ModQueue.objects.create(
            content_type=content_type,
            request_type=ADDITION,
            request={
                'name': 'test',
                'description': 'test',
                'service_id': 'test'
            },
            submitted_by=self.user)

        # Check that anonymous users can't access mod requests
        response = self.client.post(reverse('modqueue-list'))
        self.assertEqual(response.status_code, 401)

        # Check that other users can't access mod requests
        self.client.login(username=self.other_user.username, password='test')
        response = self.client.post(reverse('modqueue-list'))
        self.assertEqual(response.status_code, 403)

        # Check that the submitter can't post requests (nor approvals)
        self.client.login(username=self.user.username, password='test')
        response = self.client.post(reverse('modqueue-list'))
        self.assertEqual(response.status_code, 403)
        response = self.client.post(
            reverse('modqueue-approve', args=(request.id,)))
        self.assertEqual(response.status_code, 403)

        # Check that moderators can access mod requests
        self.client.login(username=self.moderator.username, password='test')
        response = self.client.post(
            reverse('modqueue-approve', args=(request.id,)))
        self.assertEqual(response.status_code, 200)

    def test_delete(self):
        content_type = ContentType.objects.get(
            app_label='api', model='service')
        request = models.ModQueue.objects.create(
            content_type=content_type,
            request_type=ADDITION,
            request={
                'name': 'test',
                'description': 'test',
                'service_id': 'test'
            },
            submitted_by=self.user)

        # Check that anonymous users can't delete mod requests
        response = self.client.delete(
            reverse('modqueue-detail', args=(request.id,)))
        self.assertEqual(response.status_code, 403)

        # Check that other users can't delete mod requests
        self.client.login(username=self.other_user.username, password='test')
        response = self.client.delete(
            reverse('modqueue-detail', args=(request.id,)))
        self.assertEqual(response.status_code, 404)

        # Check that moderators can't delete requests
        self.client.login(username=self.moderator.username, password='test')
        response = self.client.delete(
            reverse('modqueue-detail', args=(request.id,)))
        self.assertEqual(response.status_code, 403)

        # Check that the submitter can delete their request
        self.client.login(username=self.user.username, password='test')
        response = self.client.delete(
            reverse('modqueue-detail', args=(request.id,)))
        self.assertEqual(response.status_code, 204)

    def test_approval(self):
        content_type = ContentType.objects.get(
            app_label='api', model='service')
        request = models.ModQueue.objects.create(
            content_type=content_type,
            request_type=ADDITION,
            request={
                'name': 'test',
                'description': 'test',
                'service_id': 'test'
            },
            submitted_by=self.user)

        self.client.login(username=self.moderator.username, password='test')
        response = self.client.post(
            reverse('modqueue-approve', args=(request.id,)))
        self.assertEqual(response.status_code, 200)

    def test_disapproval(self):
        content_type = ContentType.objects.get(
            app_label='api', model='service')
        request = models.ModQueue.objects.create(
            content_type=content_type,
            request_type=ADDITION,
            request={
                'name': 'test',
                'description': 'test',
                'service_id': 'test'
            },
            submitted_by=self.user)

        self.client.login(username=self.moderator.username, password='test')
        response = self.client.post(
            reverse('modqueue-disapprove', args=(request.id,)))
        self.assertEqual(response.status_code, 200)


class UserViewSetTestCase(APITestCase):
    def setUp(self):
        super().setUp()

        self.moderator = User.objects.create_user(
            username='mod', email='test@test', password='test')
        self.user = User.objects.create_user(
            username='test', email='test@test', password='test')
        self.other_user = User.objects.create_user(
            username='test2', email='test@test', password='test')
        mod_group = Group.objects.create(name='Moderator')
        delete = Permission.objects.get(name='Can delete user')
        mod_group.permissions.add(delete)
        self.moderator.groups.add(mod_group)

    def test_ban(self):
        # Test anonymous
        response = self.client.delete(
            reverse('user-ban', args=(self.user.id,)))
        self.assertEqual(response.status_code, 401)

        # Test logged in user
        self.client.login(username=self.other_user.username, password='test')
        response = self.client.delete(
            reverse('user-ban', args=(self.user.id,)))
        self.assertEqual(response.status_code, 403)

        # Test moderator
        self.client.login(username=self.moderator.username, password='test')
        response = self.client.delete(
            reverse('user-ban', args=(self.user.id,)))
        self.assertEqual(response.status_code, 200)
        self.assertFalse(response.json()['is_active'])
