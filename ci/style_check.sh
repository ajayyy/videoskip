#!/bin/bash
# Expected to be run in root directory

set -eo pipefail

cd api
pipenv run lint

cd ../client
yarn run lint

cd ../extension
yarn run lint
