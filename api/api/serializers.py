from django.contrib.admin.models import CHANGE, ADDITION, LogEntry
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import ObjectDoesNotExist
from rest_framework import serializers

from . import models


class ContentTypeSerializer(serializers.ModelSerializer):

    class Meta:
        model = ContentType
        fields = ('model',)


class ModQueueContentObjectRelatedField(serializers.RelatedField):
    """Return a simpler representation of the object."""
    def to_representation(self, obj):
        if isinstance(obj, models.Service):
            data = ServiceSerializer(obj, context=self.context).data
        elif isinstance(obj, models.TimeType):
            data = TimeTypeSerializer(obj, context=self.context).data
        else:
            raise Exception('Unexpected type of tagged object: ' + str(obj))

        data.pop('id', None)
        data.pop('url', None)

        return data


class ModQueueSerializer(serializers.HyperlinkedModelSerializer):

    content_type = ContentTypeSerializer()
    content_object = ModQueueContentObjectRelatedField(read_only=True)

    class Meta:
        model = models.ModQueue
        fields = ('id', 'url', 'content_type', 'object_id', 'content_object',
                  'request', 'request_type', 'submitted_by', 'submitted_on',
                  'approved', 'approved_by', 'approved_on', 'reason')


class ModQueueSerializerMixin:
    def log_moderator_action(self, request, instance, validated_data, action):
        LogEntry.objects.log_action(
            user_id=request.user.id,
            content_type_id=ContentType.objects.get_for_model(instance).pk,
            object_id=instance.id,
            object_repr=repr(instance),
            action_flag=action,
            change_message=str(validated_data))

    def create_mod_queue(self, request_type, validated_data, **kwargs):
        request = self.context.get('request')
        content_type = ContentType.objects.get(
            app_label='api', model=self.Meta.model._meta.model_name)
        mod_queue = models.ModQueue.objects.create(
            content_type=content_type,
            request_type=request_type,
            request=validated_data,
            submitted_by=request.user,
            **kwargs)
        return mod_queue

    def update(self, instance, validated_data):
        request = self.context.get('request')

        if request.user.groups.filter(name='Moderator').exists():
            self.log_moderator_action(request, instance, validated_data, CHANGE)
            return super().update(instance, validated_data)

        return self.create_mod_queue(
            CHANGE, validated_data, object_id=instance.id)

    def to_representation(self, obj):
        if isinstance(obj, self.Meta.model):
            return super().to_representation(obj)

        return ModQueueSerializer(obj, context=self.context).data


class ServiceSerializer(ModQueueSerializerMixin,
                        serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.Service
        fields = ('id', 'url', 'service_id', 'name', 'description')

    def create(self, validated_data):
        """If a user is not a moderator, create a mod request"""
        request = self.context.get('request')

        if request.user.groups.filter(name='Moderator').exists():
            service = models.Service.objects.create(
                service_id=validated_data['service_id'],
                name=validated_data['name'],
                description=validated_data['description'],
                user=request.user)

            self.log_moderator_action(request, service, validated_data,
                                      ADDITION)
            return service

        return self.create_mod_queue(ADDITION, validated_data)


class TimestampSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.Timestamp
        fields = ('id', 'time_type', 'start', 'end')


class TimestampRevisionSerializer(serializers.HyperlinkedModelSerializer):

    timestamps = TimestampSerializer(many=True)

    class Meta:
        model = models.TimestampRevision
        fields = ('id', 'url', 'timestamps', 'previous_revision',
                  'next_revision', 'video')


class TimestampRevisionForVideoSerializer(
        serializers.HyperlinkedModelSerializer):

    timestamps = TimestampSerializer(many=True)

    class Meta:
        model = models.TimestampRevision
        fields = ('timestamps',)


class TimeTypeSerializer(ModQueueSerializerMixin,
                         serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.TimeType
        fields = ('id', 'url', 'name', 'description')

    def create(self, validated_data):
        """If a user is not a moderator, create a mod request"""
        request = self.context.get('request')

        if request.user.groups.filter(name='Moderator').exists():
            time_type = models.TimeType.objects.create(
                name=validated_data['name'],
                description=validated_data['description'],
                user=request.user)

            self.log_moderator_action(request, time_type, validated_data,
                                      ADDITION)
            return time_type

        return self.create_mod_queue(ADDITION, validated_data)


class VideoSerializer(serializers.HyperlinkedModelSerializer):

    timestamp_revision = TimestampRevisionForVideoSerializer(required=False)

    class Meta:
        model = models.Video
        fields = ('id', 'url', 'video_id', 'service', 'timestamp_revision')

    def to_representation(self, obj):
        """Move fields from timestamp_revision to video representation."""
        representation = super().to_representation(obj)
        timestamp_representation = representation.pop('timestamp_revision')
        representation['timestamps'] = timestamp_representation['timestamps']
        return representation

    def to_internal_value(self, data):
        """Move back fields to timestamp_revision."""
        if 'timestamps' in data:
            data['timestamp_revision'] = {
                'timestamps': data.pop('timestamps')
            }
        internal = super().to_internal_value(data)
        return internal

    def get_initial(self):
        initial = super().get_initial()
        initial.pop('timestamp_revision', None)
        initial['timestamps'] = [{
            'time_type': None,
            'start': 0,
            'end': 0
        }]
        return initial

    def validate(self, attrs):
        request = self.context.get('request')

        # Make sure there is at least one timestamp
        if 'timestamp_revision' not in attrs:
            raise serializers.ValidationError(
                'The key timestamps is required.')

        # Make sure the timestamp list is not empty
        if not attrs['timestamp_revision']['timestamps']:
            raise serializers.ValidationError(
                'The timestamps key must be a list and not empty.')

        if request.method == 'POST':
            # Make sure video does not already exist
            try:
                models.Video.objects.get(video_id=attrs['video_id'],
                                         service=attrs['service'])
                raise serializers.ValidationError(
                    'Video with id and service already exists. '
                    'Use a PUT or PATCH request.')
            except ObjectDoesNotExist:
                pass

        for timestamp in attrs['timestamp_revision']['timestamps']:
            # Make sure a time type exists
            if 'time_type' not in timestamp:
                raise serializers.ValidationError(
                    'A timetype key is required')
            # Make sure the start time is less than the end time
            if timestamp['start'] >= timestamp['end']:
                raise serializers.ValidationError(
                    'Start time must be less than end time')
        return attrs

    def order_timestamps(self, timestamps):
        """Order timestamps by start time

        This avoids needing to order timestamps in the database during
        verification (if we didn't initially order them), and during GETs.
        """
        timestamps.sort(key=lambda timestamp: timestamp['start'])

    def update(self, instance, validated_data):
        timestamps = validated_data.pop('timestamp_revision')['timestamps']
        self.order_timestamps(timestamps)

        timestamp_revision = instance.timestamp_revision

        # Basic check to see that timestamps are not the same
        different = False
        if len(timestamps) == timestamp_revision.timestamps.count():
            for i, timestamp in enumerate(timestamp_revision.timestamps.all()):
                if (timestamp.time_type != timestamps[i]['time_type'] or
                        timestamp.start != timestamps[i]['start'] or
                        timestamp.end != timestamps[i]['end']):
                    different = True
                    break
        else:
            different = True

        # create new timestamp revision
        if different:
            request = self.context.get('request')
            new_timestamp_revision = models.TimestampRevision.objects.create(
                user=request.user,
                previous_revision=timestamp_revision,
                video=instance)
            for timestamp in timestamps:
                models.Timestamp.objects.create(
                    start=timestamp['start'],
                    end=timestamp['end'],
                    time_type=timestamp['time_type'],
                    timestamp_revision=new_timestamp_revision)
            instance.timestamp_revision = new_timestamp_revision

        # set any remain values
        for key, value in validated_data.items():
            if getattr(instance, key, None) != value:
                setattr(instance, key, value)

        instance.save()
        return instance

    def create(self, validated_data):
        request = self.context.get('request')

        # Create first timestamp revision
        timestamp_revision = models.TimestampRevision.objects.create(
            user=request.user)

        timestamps = validated_data['timestamp_revision']['timestamps']
        self.order_timestamps(timestamps)

        # Create timestamps for timestamp revision
        for timestamp in timestamps:
            models.Timestamp.objects.create(
                start=timestamp['start'],
                end=timestamp['end'],
                time_type=timestamp['time_type'],
                timestamp_revision=timestamp_revision)

        # Create video
        video = models.Video.objects.create(
            video_id=validated_data['video_id'],
            service=validated_data['service'],
            timestamp_revision=timestamp_revision,
            user=request.user)

        timestamp_revision.video = video
        timestamp_revision.save()

        return video


class UserSerializer(serializers.HyperlinkedModelSerializer):

    is_moderator = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = ('id', 'url', 'username', 'is_moderator', 'is_active')

    def get_is_moderator(self, obj):
        return obj.groups.filter(name='Moderator').exists()


class ApprovalSerializer(serializers.Serializer):
    reason = serializers.CharField(max_length=1024, required=False, default='')
