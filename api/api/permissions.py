from rest_framework import permissions
from rest_framework.permissions import DjangoModelPermissions


class ModeratorActionPermission(DjangoModelPermissions):
    """Read only, except actions can be done by moderators."""
    def has_permission(self, request, view):
        if request.method in permissions.SAFE_METHODS:
            return True

        if super().has_permission(request, view) and view.action in ['ban']:
            return True


class ModifyAllButDeleteByOwner(DjangoModelPermissions):
    """Allow users to POST, PUT, PATCH. Allow owners to DELETE"""
    def has_permission(self, request, view):
        if super().has_permission(request, view):
            return True

        if request.method in permissions.SAFE_METHODS:
            return True

        if request.user and not request.user.is_authenticated:
            return False

        return True

    def has_object_permission(self, request, view, obj):
        if super().has_permission(request, view):
            return True

        if request.method in permissions.SAFE_METHODS:
            return True

        if (request.method == 'DELETE' and
                request.user != obj.user):
            return False

        return True


class ReadOnlyAndDeleteByOwner(DjangoModelPermissions):
    """Allow owner to DELETE"""
    def has_permission(self, request, view):
        if super().has_permission(request, view):
            return True

        if request.method in permissions.SAFE_METHODS:
            return True

        if request.user and not request.user.is_authenticated:
            return False

        return True

    def has_object_permission(self, request, view, obj):
        if super().has_permission(request, view):
            return True

        if request.method in permissions.SAFE_METHODS:
            return True

        if (request.method == 'DELETE' and
                request.user == obj.user):
            return True

        return False


class AddByAllAndDeleteByOwner(DjangoModelPermissions):
    """Allow users to POST. Allow owner to DELETE"""
    def has_permission(self, request, view):
        if super().has_permission(request, view):
            return True

        if request.method in permissions.SAFE_METHODS:
            return True

        if request.user and not request.user.is_authenticated:
            return False

        return True

    def has_object_permission(self, request, view, obj):
        if super().has_permission(request, view):
            return True

        if request.method in permissions.SAFE_METHODS:
            return True

        if (request.method == 'DELETE' and
                request.user != obj.user):
            return False

        return True


class ModQueuePermissions(DjangoModelPermissions):
    """Allow only moderators except allow users for their own requests.

    Note that the queryset in the viewset filters the queryset if the
    user is not a moderator.
    """

    def has_permission(self, request, view):
        # This is mostly here just to show the API buttons to the user
        if request.method != 'POST':
            return True

        return super().has_permission(request, view)

    def has_object_permission(self, request, view, obj):
        # Check if POST so that users can't approve of their own request
        # (Also allows them to change or delete their request)
        if request.user == obj.submitted_by and request.method != 'POST':
            return True

        return super().has_permission(request, view)
