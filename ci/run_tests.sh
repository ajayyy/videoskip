#!/bin/bash
# Expected to be run in root directory

set -eo pipefail

export DJANGO_SETTINGS_MODULE=videoskip.settings_test

cd api

pipenv run python ./manage.py migrate --noinput
pipenv run coverage run ./manage.py test --noinput \
  --testrunner="xmlrunner.extra.djangotestrunner.XMLTestRunner"
pipenv run coverage html --include=api/\*.py --omit=api/migrations/\*.py

# TODO add client and extension tests.
