export const state = () => ({
  username: null,
  id: null,
  is_moderator: false
})

export const mutations = {
  set (state, data) {
    state.username = data.username
    state.id = data.id
    state.is_moderator = data.is_moderator
  }
}
