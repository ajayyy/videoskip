FROM python:alpine AS videoskip-api

ENV PYTHONUNBUFFERED 1
ENV PIPENV_VENV_IN_PROJECT 1

RUN apk add --no-cache uwsgi-python3

RUN mkdir /code
WORKDIR /code

COPY api/Pipfile* /code/

RUN pip install pipenv

RUN apk add --no-cache postgresql-libs linux-headers

RUN \
 apk add --no-cache --virtual .build-deps gcc musl-dev postgresql-dev pcre-dev && \
 pipenv install && \
 apk --purge del .build-deps

COPY api /code/

# These don't get written to the image. They are just used so python does not crash.
# (I wanted to avoid os.environ.get() so that it fails fast in normal situations.)
ARG SECRET_KEY=tmp
ARG POSTGRES_PASSWORD=tmp
ARG CLIENT_URL=tmp//tmp
ARG EMAIL_HOST=tmp
ARG EMAIL_USER=tmp
ARG EMAIL_PASSWORD=tmp
ARG ADMIN_EMAIL=tmp

RUN pipenv run python manage.py collectstatic --settings videoskip.settings_prod --noinput

###############################################################################

FROM node:alpine AS videoskip-client

RUN npm install -g yarn

RUN mkdir /code
WORKDIR /code

COPY client /code

RUN yarn install --production

ENV API_URL /api

RUN yarn run build

ENV HOST 0.0.0.0

###############################################################################

FROM nginx:alpine AS videoskip

COPY docker/nginx.conf /etc/nginx/conf.d/default.template

ADD https://raw.githubusercontent.com/certbot/certbot/master/certbot-nginx/certbot_nginx/tls_configs/options-ssl-nginx.conf /etc/letsencrypt-static/options-ssl-nginx.conf
ADD https://raw.githubusercontent.com/certbot/certbot/master/certbot/ssl-dhparams.pem /etc/letsencrypt-static/ssl-dhparams.pem

RUN mkdir /code
WORKDIR /code

COPY --from=videoskip-api /code/static static

