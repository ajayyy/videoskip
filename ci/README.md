CI Scripts
==========

Contains some scripts that run on the CI.

API code coverage can be found [here](https://videoskip.gitlab.io/videoskip/coverage/).
